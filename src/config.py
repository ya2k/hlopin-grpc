import yaml

config = None

with open("config/config.yaml", "r") as stream:
  config = yaml.safe_load(stream)

