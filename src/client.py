import sys

import grpc
import numpy as np

import matplotlib.pyplot as plt

from mpl_toolkits import mplot3d
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton, QWidget, QLineEdit, QSlider
from PyQt5.QtCore import Qt

import data_service_pb2_grpc
import data_service_pb2

from config import config


data = []

class GUI():
  @classmethod
  def init(klass):
    app = QApplication(sys.argv)
    gui = klass.MainWindow()
    app.exec()

    return gui

  class MainWindow(QMainWindow):
    def __init__(self):
      super().__init__()

      self.initGUI()

    def initGUI(self):
      self.setGeometry(100, 100, 1350, 1000)
      self.setWindowTitle("gRPC client GUI")

      self.fig = plt.figure()
      self.canvas = FigureCanvas(self.fig)
      self.axes = self.fig.add_subplot(111, projection='3d')
      self.canvas.resize(600, 600)
      self.setCentralWidget(self.canvas)

      self.label_xmin = QLabel(self, text = "x_min")
      self.label_xmin.move(20, 10)
      self.textbox_xmin = QLineEdit(self)
      self.textbox_xmin.move(20, 40)
      self.textbox_xmin.resize(60, 20)
      self.textbox_xmin.setText("1")

      self.label_xmax = QLabel(self, text = "x_max")
      self.label_xmax.move(100, 10)
      self.textbox_xmax = QLineEdit(self)
      self.textbox_xmax.move(100, 40)
      self.textbox_xmax.resize(60, 20)
      self.textbox_xmax.setText("10")

      self.label_xstep = QLabel(self, text = "x_step")
      self.label_xstep.move(180, 10)
      self.textbox_xstep = QLineEdit(self)
      self.textbox_xstep.move(180, 40)
      self.textbox_xstep.resize(60, 20)
      self.textbox_xstep.setPlaceholderText("x_step")
      self.textbox_xstep.setText("1")

      self.label_ymin = QLabel(self, text = "y_min")
      self.label_ymin.move(20, 55)
      self.textbox_ymin = QLineEdit(self)
      self.textbox_ymin.move(20, 80)
      self.textbox_ymin.resize(60, 20)
      self.textbox_ymin.setPlaceholderText("y_min")
      self.textbox_ymin.setText("1")

      self.label_ymax = QLabel(self, text = "y_max")
      self.label_ymax.move(100, 55)
      self.textbox_ymax = QLineEdit(self)
      self.textbox_ymax.move(100, 80)
      self.textbox_ymax.resize(60, 20)
      self.textbox_ymax.setPlaceholderText("y_max")
      self.textbox_ymax.setText("10")

      self.label_ystep = QLabel(self, text = "y_step")
      self.label_ystep.move(180, 55)
      self.textbox_ystep = QLineEdit(self)
      self.textbox_ystep.move(180, 80)
      self.textbox_ystep.resize(60, 20)
      self.textbox_ystep.setPlaceholderText("y_step")
      self.textbox_ystep.setText("1")

      self.label_t = QLabel(self, text = "t")
      self.label_t.move(20, 95)
      self.textbox_t = QLineEdit(self)
      self.textbox_t.move(20, 120)
      self.textbox_t.resize(60, 20)
      self.textbox_t.setPlaceholderText("t")
      self.textbox_t.setText("10")

      self.label_tstep = QLabel(self, text = "t_step")
      self.label_tstep.move(100, 95)
      self.textbox_tstep = QLineEdit(self)
      self.textbox_tstep.move(100, 120)
      self.textbox_tstep.resize(60, 20)
      self.textbox_tstep.setPlaceholderText("t_step")
      self.textbox_tstep.setText("1")

      self.label_slider = QLabel(self, text = "0")
      self.label_slider.move(300, 40)
      self.slider = QSlider(Qt.Horizontal, self)
      self.slider.move(300, 20)
      self.slider.setMinimum(0)
      self.slider.setMaximum(100)
      self.slider.setMinimumWidth(800)
      self.slider.valueChanged.connect(self.on_slider_change)

      self.button = QPushButton(self)
      self.button.move(20, 160)
      self.button.setText("process")
      self.button.clicked.connect(self.on_button_click)

      self.show()

    def on_button_click(self):
      global data
      data = []

      sliderMax = int(float(self.textbox_t.text()) / float(self.textbox_tstep.text()))
      self.slider.setMaximum(sliderMax)

      for index, chunk in enumerate(make_request(self.params())):
        if index == 1: self.set_plot(0)

        data.append(chunk)

    def on_slider_change(self):
      global data
 
      value = self.slider.value()
      self.label_slider.setText("{:.2f}".format(value * float(self.textbox_tstep.text())))
      value -= 1

      if value < 0 or len(data) <= value: return

      self.set_plot(value)

    def set_plot(self, index):
      X = np.arange(float(self.textbox_xmin.text()), float(self.textbox_xmax.text()), float(self.textbox_xstep.text()))
      Y = np.arange(float(self.textbox_ymin.text()), float(self.textbox_ymax.text()), float(self.textbox_ystep.text()))
      XX, YY = np.meshgrid(X, Y)

      self.axes.clear()
      self.axes.plot_surface(XX, YY, data[index], color="0.9")

      plt.draw()


    def params(self):
      return  {
        "x_min": float(self.textbox_xmin.text()),
        "x_max": float(self.textbox_xmax.text()),
        "x_step": float(self.textbox_xstep.text()),
        "y_min": float(self.textbox_ymin.text()),
        "y_max": float(self.textbox_ymax.text()),
        "y_step": float(self.textbox_ystep.text()),
        "time": float(self.textbox_t.text()),
        "time_step": float(self.textbox_tstep.text())
      }

def make_request(params):
  with grpc.insecure_channel(config["socket"], options=(("grpc.enable_http_proxy", 0),)) as channel:
    try: grpc.channel_ready_future(channel).result(timeout=10)
    except grpc.FutureTimeoutError: sys.exit(1)

    stub = data_service_pb2_grpc.DataServiceStub(channel)
    request = data_service_pb2.DataModelRequest(**params)

    for result in stub.getResult(request):
      result = list(map(lambda x : x.array, result.data))
      result = np.asarray(result)

      yield result

def run():
  GUI.init()


if __name__ == '__main__':
    run()
