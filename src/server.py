import logging, grpc, math, time, sys
from concurrent import futures

import numpy as np

import data_service_pb2_grpc
import data_service_pb2

from config import config

def main():
  setup_logger()
  bind_socket()

def setup_logger():
  logging.basicConfig(
    stream = sys.stdout,
    level = logging.DEBUG,
    format = "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
  )

def bind_socket():
  logger = logging.getLogger("gRPC server")
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  data_service_pb2_grpc.add_DataServiceServicer_to_server(ExampleServer(logger), server)

  server.add_insecure_port(config["socket"])
  server.start()
  logger.info(f"live at {config['socket']}")

  server.wait_for_termination()


class ExampleServer(data_service_pb2_grpc.DataServiceServicer):
  def __init__(self, logger):
    self.logger = logger

  def getResult(self, request, context):
    self.logger.info("Received request")

    x_min = request.x_min
    x_max = request.x_max
    x_step = request.x_step
    y_min = request.y_min
    y_max = request.y_max
    y_step = request.y_step
    time = request.time
    time_step = request.time_step

    x_space = np.arange(x_min, x_max, x_step)
    y_space = np.arange(y_min, y_max, y_step)
    x, y = np.meshgrid(x_space, y_space)

    print(time, time_step)
    t_space = np.arange(0, time, time_step).tolist()
    for index, t in enumerate(t_space):
      z = self.z(x, y, t)


      data = list(map(lambda x : data_service_pb2.DoubleArray(array=x), z))
      yield data_service_pb2.DataModelResponse(data=data)

      self.logger.info(f"Rendered stream chunk {index + 1}/{len(t_space)}")
    self.logger.info("Rendered response")

  def z(self, x, y, t):
    return np.sin(x + y + t)

if __name__ == '__main__': main()
