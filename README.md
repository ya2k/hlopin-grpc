`pip`, `venv`, `python ~> 3`

Linux, macOS:

**Перекомпилияция pb**

`(cd definitions && python -m grpc_tools.protoc -I. data_service.proto --python_out=../src --grpc_python_out=../src)`

**Локальное развертывание**

Для корректной работы необходим интепретатор питона мажорной версии 3:

`python --version # ~> 3`

Необходимо установить зависимости:

`pip install -r requirements.txt`

или в виртуальном окружении:

`python -m venv venv && source venv/bin/activate`

***Запуск сервера***

`python src/server.py`

Для включения логгинга gRPC-сервера (для дебага) надо запустить с env-переменными:

`GRPC_VERBOSITY=DEBUG GRPC_TRACE=http python src/server.py`

***Запуск клиента***

`python src/client.py`
